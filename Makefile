# Makefile for eselect-syntax

distapp = eselect-syntax
distver := $(shell date -u +%Y%m%d)
distpkg := $(distapp)-$(distver)

PREFIX = ${HOME}/.vim/

files_vars = \
	files_doc \
	files_ftdetect \
	files_ftplugin \
	files_indent \
	files_plugin \
	files_syntax

files_doc = \
	doc/eselect-syntax.txt

files_ftdetect = \
	ftdetect/eselect.vim

files_ftplugin = \
	ftplugin/eselect.vim

files_indent = \
	indent/eselect.vim

files_plugin = \
	plugin/neweselect.vim

files_syntax = \
	syntax/eselect.vim

files = $(foreach f, $(files_vars), $($(f)) )

scripts: ${files}

install: install-dirs install-files

install-dirs: $(foreach a, $(sort $(dir $(files))), \
	install-dir-$(a))

install-files: $(foreach a, $(sort $(files)), \
	install-file-$(subst /,_,$(a) ))

install-dir-%:
	mkdir -p "$(PREFIX)/$*"

install-file-%: $(subst _,/,$*)
	cp "$(subst _,/,$*)" "$(PREFIX)/$(subst _,/,$*)"

uninstall : uninstall-files

uninstall-files: $(foreach a, $(sort $(files)), \
	uninstall-file-$(subst /,_,$(a) ))

uninstall-file-%: $(subst _,/,$*)
	[ ! -f "$(PREFIX)/$(subst _,/,$*)" ] || rm "$(PREFIX)/$(subst _,/,$*)"

tag:
	git pull
	git tag $(distpkg)
	@echo
	@echo "tag created remember to push it"
	@echo

dist: tag
	git archive --prefix=$(distpkg)/ --format=tar -o $(distpkg).tar $(distpkg)
	mkdir $(distpkg)/
	git log > $(distpkg)/ChangeLog
	tar vfr $(distpkg).tar $(distpkg)/ChangeLog
	bzip2 $(distpkg).tar
	rm -rf $(distpkg)/

dist-upload: dist
	scp $(distpkg).tar.bz2 dev.gentoo.org:/space/distfiles-local/

clean:
	find . -name '*~' | xargs rm || true

